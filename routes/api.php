<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

//Route::post('upload', 'UploadInfoController@upload');
Route::post('save-info', 'UploadInfoController@saveInfo');

Route::get('test/{id}', 'CertificateController@sendEmailCertificate');

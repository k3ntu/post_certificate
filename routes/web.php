<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('storage/{filename}', function ($filename)
{
    return \Image::make(storage_path('public/' . $filename))->response();
});*/

Route::get('/testing', 'UploadInfoController@searchMissing');

Route::get('mailable', function () {
    $invoice = App\Certificate::find(520);

    return new App\Mail\CertificateShipped($invoice);
});

Route::get('test/{id}', 'CertificateController@sendEmailCertificate');


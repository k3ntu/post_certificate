<?php

namespace App\Http\Controllers;

use App\Certificate;
use App\Mail\CertificateShipped;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CertificateController extends Controller
{
    public function sendEmailCertificate(Request $request, $id)
    {
        try {
            $certificate = Certificate::findOrFail($id);

            Mail::to($certificate->email)
                //->bcc('capacitacion@cpsp.pe')
                ->send(new CertificateShipped($certificate));

            return response()->json([
                'state' => 'success',
                'id' => $certificate->id
            ]);
        } catch (ModelNotFoundException $error) {
            return response()->json([
                'state' => 'failed',
                'message' => $error->getMessage()
            ]);
        }

    }
}

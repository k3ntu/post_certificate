<?php

namespace App\Http\Controllers;

use App\Certificate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UploadInfoController extends Controller
{
    /*public function upload(Request $request)
    {
        $img = $request->get('image', false);
        $filename = 'temp_image';

        if ($img) {
            $resource = \Image::make($img)->encode('png');
            Log::debug("Storing image");
            if ($resource !== false) {
                Storage::put("public/{$filename}.png", (string) $resource);
            } else {
                Log::error("Failed to get resource from string");
            }
        }

        return response()->json([
            'message' => 'success',
            'name' => $filename
        ]);
    }*/

    public function searchMissing()
    {
        $certicates = Certificate::all();

        foreach ($certicates as $cer) {
            $url = Storage::disk('public')->has($cer->token.'.pdf');;
            if (!$url) {
                return response()->json($cer);
            }
        }
        return response()->json([
            'status' => 'success',
            'message' => 'No hay errores en la integradad de los datos'
        ]);
    }

    public function upload(Request $request)
    {
        $pdf = $request->input('data', false)['pdf'];
        $filename = 'temp_image';

        if ($pdf) {
            $data = base64_decode($pdf);
            Log::debug("Storing image");
            Storage::put("public/{$filename}.pdf", $data);
        }

        return response()->json([
            'message' => 'success',
            'name' => $filename
        ]);
    }

    public function saveInfo(Request $request)
    {
        $data = $request->input('data');

        $certificate = new Certificate();

        $certificate->name = $data['name'];
        $certificate->email = $data['email'];
		$certificate->dni = $data['dni'];
        $certificate->token = $data['token'];
        $certificate->nameTheme = $data['nameTheme'];
        $certificate->hours = $data['hours'];

        if ($certificate->save()) {
            return response()->json([
                'state' => 'success',
                'message' => 'Guardado certificado #' . $certificate->id,
            ]);
        } else {
            return  response()->json([
                'state' => 'failed',
                'message' => 'No se pudo guardar',
            ]);
        }
    }
}

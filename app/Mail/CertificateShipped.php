<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class CertificateShipped extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('web@cpsp.pe')
                    ->subject('Certificado digital del CPsP')
                    ->markdown('emails.certificate.call')
                    ->attachFromStorageDisk('public', $this->data['token'] .'.pdf');
        /*return $this->from('colegiodepsicologos.prensa@gmail.com')
                    ->bcc(['web_cdn@cpsp.pe'])
                    ->subject('#' . $this->data['id'] . ' | Bingo Show por el día del psicólogo - CDR-I Lima y Callao, del CPsP')
                    ->markdown('emails.certificate.call')
                    ->attachFromStorageDisk('public', 'bingo_del_cdr1.jpeg')
                    ->attachFromStorageDisk('public', $this->data['token']);*/
    }
}

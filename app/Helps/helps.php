<?php

use Carbon\Carbon;

function conDateOrCurrent($value) {
    $months = [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septimbre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    ];

    $date = Carbon::now();

    if ($value != '') {
        $text = $date->day . ' de ' . $months[$date->month];
    } else {
        $date = Carbon::parse($value);
        $text = $months[$date->month] . ' de ' . $date->year;
    }

    return $text;
}

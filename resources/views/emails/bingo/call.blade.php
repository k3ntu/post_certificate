@component('mail::message')
    # Estimada Psicólogo(a) {{ $data['name'] }}

    El Consejo Directivo Regional I - Lima y Callao del Colegio de Psicólogos del Perú agradece su inscripción en este gran Bingo Show, organizado para todos los colegiados pertenecientes al CDR-I por el Día Del Psicólogo.
    A continuación adjuntamos la cartilla gratuita que se le ha asignado, con un número único de identificación. Además dejaremos algunas pautas para el día del evento:

    # ---

    BINGOSHOW - Modalidad Virtual:
    * El CDR-I Lima y Callao transmitirá el bingo show en vivo a través de su plataforma de Facebook Live, el día 30 de abril del 2021 a las 7:00 p.m.
    * Para poder jugar debe estar conectada/o a la transmisión.
    * En caso usted gane el juego, debe escribir BINGO en la transmisión en vivo. En ese momento el encargado parará el juego y se contactará con usted para que pueda ingresar a la plataforma ZOOM. De esta manera, verificaremos en vivo que usted sea el ganador, y que cumpla con los requisitos establecidos: Ser psicólogo/a colegiado/a, pertenecer al CDR-I Lima y Callao y que su cartilla sea la misma que se le envió por correo electrónico.
@endcomponent


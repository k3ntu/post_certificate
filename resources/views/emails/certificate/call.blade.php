@component('mail::message')
# Hola {{ $data['name'] }}

Por el medio presente le hacemos, la entrega del certificado digital del tema: "{{ $data['nameTheme'] }}" por el valor de {{ $data['hours'] }} hrs. acádemicas.

Nota: Durante las proximas 48h. Se enviaran los restantes que le faltan.

Un abrazo calido y feliz día,<br>
CPsP
@endcomponent

